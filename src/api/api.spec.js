// import libraries
import {
	getAllPublicMessages,
	getPrivateMessages,
	getUserByID,
	getPrivateConversation,
	createPrivateMessage,
	createPublicMessage,
	getUserLastMessage,
} from ".";

// import datas
const userList = require("../datas/users.json");
const messageList = require("../datas/messages.json");

const mockPublicMessages = [
	{
		_id: "msg_4xjvy11jlkgmf7a98",
		date: "2020-09-10T03:37:17.217Z",
		isPublic: true,
		transmitter: "usr-4xjvy11jlkgmf7a8s",
		recipient: null,
		content: "In aliquam qui commodi et quia cum eius.",
	},
	{
		_id: "msg_4xjvy11jlkgmf7a99",
		date: "2020-04-09T10:24:09.132Z",
		isPublic: true,
		transmitter: "usr-4xjvy11jlkgmf7a8q",
		recipient: null,
		content: "Suscipit ratione ipsum et eos velit.",
	},
];

const mockPrivateMessage = [
	{
		_id: "msg_4xjvy11jlkgmf7a95",
		date: "2019-11-24T09:13:47.190Z",
		isPublic: false,
		transmitter: "usr-4xjvy11jlkgmf7a8w",
		recipient: "usr-4xjvy11jlkgmf7a90",
		content: "Id aut nemo consectetur non tempora ea.",
	},
	{
		_id: "msg_4xjvy11jlkgmf7a96",
		date: "2020-08-01T19:07:23.182Z",
		isPublic: false,
		transmitter: "usr-4xjvy11jlkgmf7a8z",
		recipient: "usr-4xjvy11jlkgmf7a92",
		content: "Eum sed dolorem voluptatem expedita reprehenderit voluptatem laboriosam nam.",
	},
	{
		_id: "msg_4xjvy11jlkgmf7a97",
		date: "2020-08-01T19:07:23.182Z",
		isPublic: false,
		transmitter: "usr-4xjvy11jlkgmf7a8z",
		recipient: "usr-4xjvy11jlkgmf7a8w",
		content: "Eum sed dolorem voluptatem expedita reprehenderit voluptatem laboriosam nam.",
	},
];

const mockUser = userList[0];
const mockMessage = messageList[0];

// tests suites
describe("getAllPublicMessages", () => {
	let publicMessages;

	it("should render an empty array if there is no message", () => {
		publicMessages = getAllPublicMessages([]);
		expect(publicMessages).toEqual([]);
	});

	it("should render an empty array if there is no public message", () => {
		const testingMessageList = [mockPrivateMessage[0], mockPrivateMessage[1]];
		publicMessages = getAllPublicMessages(testingMessageList);
		expect(publicMessages).toEqual([]);
	});

	it("should render an array with one public message if there is one passed", () => {
		const testingMessageList = [mockPublicMessages[0], mockPrivateMessage[0]];
		publicMessages = getAllPublicMessages(testingMessageList);
		expect(publicMessages.length).toEqual(1);
	});

	it("should render an array of uniques public messages", () => {
		publicMessages = getAllPublicMessages(messageList);
		publicMessages.map((message) => expect(message.isPublic).toEqual(true));
	});

	it("should render an array with all public messages", () => {
		publicMessages = getAllPublicMessages([mockPublicMessages[0], mockPublicMessages[1]]);
		expect(publicMessages.length).toEqual(mockPublicMessages.length);
	});
});

describe("getPrivateMessages", () => {
	let privateMessages;
	const userID = "usr-4xjvy11jlkgmf7a8w";

	it("should render an empty array if there is no message", () => {
		privateMessages = getPrivateMessages([], userID);
		expect(privateMessages).toEqual([]);
	});

	it("should render an empty array if there is no private message", () => {
		privateMessages = getPrivateMessages(
			[mockPublicMessages[0], mockPublicMessages[1]],
			userID
		);
		expect(privateMessages).toEqual([]);
	});

	it("should render an empty array if user is not transmitter or recipient of private message", () => {
		privateMessages = getPrivateMessages(
			[mockPrivateMessage[1], mockPublicMessages[0]],
			userID
		);
		expect(privateMessages.length).toEqual(0);
	});

	it("should render an array with one private message if there is one passed and user is transmitter or recipient", () => {
		privateMessages = getPrivateMessages(
			[mockPrivateMessage[0], mockPublicMessages[0]],
			userID
		);
		expect(privateMessages.length).toEqual(1);
	});

	it("should render an array of uniques privates messages where user is transmitter or recipient", () => {
		privateMessages = getPrivateMessages(messageList, userID);
		privateMessages.map((message) =>
			expect(message.transitter || message.recipient === userID)
		);
	});

	it("should render an array with all private message where user is transmitter or recipient", () => {
		privateMessages = getPrivateMessages(
			[mockPrivateMessage[0], mockPrivateMessage[2]],
			userID
		);
		expect(privateMessages.length).toEqual(2);
	});
});

describe("getUserByID", () => {
	const userID = userList[0]._id;

	it("should render null if no arguments are passed", () => {
		const user = getUserByID();
		expect(user).toEqual(null);
	});

	it("should render null if user is not found", () => {
		const user = getUserByID(userList, "foo");
		expect(user).toEqual(null);
	});

	it("should render an object with user properties if user exist", () => {
		const user = getUserByID(userList, userID);
		expect(user).toEqual(mockUser);
	});
});

describe("getPrivateConversation", () => {
	let privateConversation;
	const currentUserID = "usr-4xjvy11jlkgmf7a8w";
	const distantUserID = "usr-4xjvy11jlkgmf7a8z";
	const privateMessage = {
		_id: "msg_4xjvy11jlkgmf7a97",
		date: "2020-08-01T19:07:23.182Z",
		isPublic: false,
		transmitter: "usr-4xjvy11jlkgmf7a8z",
		recipient: "usr-4xjvy11jlkgmf7a8w",
		content: "Eum sed dolorem voluptatem expedita reprehenderit voluptatem laboriosam nam.",
	};

	it("should render an empty array if no user IDs are passed", () => {
		privateConversation = getPrivateConversation(messageList);
		expect(privateConversation).toEqual([]);
	});

	it("should return an empty array if no message between the two users are found", () => {
		privateConversation = getPrivateConversation(mockPrivateMessage, "foo", "bar");
		expect(privateConversation).toEqual([]);
	});

	it("should return an array of one private message where currentUserID and distantUserID are either transmitter or recipient", () => {
		privateConversation = getPrivateConversation(
			mockPrivateMessage,
			currentUserID,
			distantUserID
		);
		expect(privateConversation.length).toEqual(1);
	});

	it("should should return an array of many private messages where currentUserID and distantUserID are either transmitter or recipient", () => {
		privateConversation = getPrivateConversation(
			[...mockPrivateMessage, privateMessage],
			currentUserID,
			distantUserID
		);
		expect(privateConversation.length).toEqual(2);
	});
});

describe("createPrivateMessage", () => {
	let privateMessage;
	const currentUserID = "usr-4xjvy11jlkgmf7a8w";
	const distantUserID = "usr-4xjvy11jlkgmf7a8z";
	const messageText = "Test private message";

	beforeEach(() => {
		privateMessage = createPrivateMessage(currentUserID, distantUserID, messageText);
	});
	afterEach(() => {
		privateMessage = null;
	});

	it("should throw an error if both user ids are not passed", () => {
		expect(() => createPrivateMessage()).toThrow();
	});
	it("should render a privateMessage object with a property content equal to sent content", () => {
		expect(privateMessage.content).toEqual(messageText);
	});
	it("should return a privateMessage object with an unique string _id", () => {
		expect(typeof privateMessage._id).toBe("string");
	});
	it("should return a privateMessage object with a date of creation", () => {
		expect(typeof privateMessage.date).toBe("string");
	});
	it("should return a privateMessage object with a transmitter property wich is equal to currentUserID", () => {
		expect(privateMessage.transmitter).toEqual(currentUserID);
	});
	it("should return a privateMessage object with a recipient property which is equal to distantUserID", () => {
		expect(privateMessage.recipient).toEqual(distantUserID);
	});
	it("should render a privateMessage object with a property isPublic of value false", () => {
		expect(privateMessage.isPublic).toBe(false);
	});
});

describe("createPublicMessage", () => {
	let publicMessage;
	const currentUserID = "usr-4xjvy11jlkgmf7a8w";
	const messageText = "Test public message";

	beforeEach(() => {
		publicMessage = createPublicMessage(currentUserID, messageText);
	});
	afterEach(() => {
		publicMessage = null;
	});

	it("should throw an error if user id is not passed", () => {
		expect(() => createPublicMessage()).toThrow();
	});
	it("should render a publicMessage object with a property content equal to sent content", () => {
		expect(publicMessage.content).toEqual(messageText);
	});
	it("should return a publicMessage object with an unique string _id", () => {
		expect(typeof publicMessage._id).toBe("string");
	});
	it("should return a publicMessage object with a date of creation", () => {
		expect(typeof publicMessage.date).toBe("string");
	});
	it("should return a publicMessage object with a transmitter property wich is equal to currentUserID", () => {
		expect(publicMessage.transmitter).toEqual(currentUserID);
	});
	it("should return a publicMessage object with a recipient property which is equal to `null`", () => {
		expect(publicMessage.recipient).toEqual(null);
	});
	it("should render a publicMessage object with a property isPublic of value `true`", () => {
		expect(publicMessage.isPublic).toEqual(true);
	});
});

describe("getUserLastMessage", () => {
	let userLastMessage;
	const wrongAuthorID = "usr-4xjvy11jlkgmf7a8w";
	const mockAuthorID = mockMessage.transmitter;

	it("should render ` ... ` if there is no message", () => {
		userLastMessage = getUserLastMessage([], wrongAuthorID);
		expect(userLastMessage).toEqual(" ... ");
	});

	it("should render ` ... ` if user has no message", () => {
		userLastMessage = getUserLastMessage([mockMessage], wrongAuthorID);
		expect(userLastMessage).toEqual(" ... ");
	});

	it("should render the message content if user has a message", () => {
		userLastMessage = getUserLastMessage([mockMessage], mockAuthorID);
		expect(userLastMessage).toEqual(mockMessage.content);
	});
});
