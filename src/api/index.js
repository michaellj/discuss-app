// import libraires
import uniqid from "uniqid";

// API FUNCTIONS

/**
 * @function getAllPublicMessages
 * @param {Array.<{_id:String,transmitter:String,recipient:(String|null),isPublic:Boolean,date:object}>} messageList contains the full list of messages
 * @returns {Array.<{_id:String,transmitter:String,recipient:null,isPublic:true,date:object}>} array of public messages
 */
const getAllPublicMessages = function (messageList) {
	const list = messageList.filter((message) => message.isPublic === true);
	return list.sort((a, b) => new Date(a.date) - new Date(b.date));
};

/**
 * @function getPrivateMessages
 * @param {Array.<{_id:String,transmitter:String,recipient:(String|null),isPublic:Boolean,date:object}>} messageList contains the full list of messages
 * @param {string} userID current user id
 * @returns {Array.<{_id:String,transmitter:String,recipient:String,isPublic:false,date:object}>} array of current user private messages
 */
const getPrivateMessages = function (messageList, userID) {
	const messageResults = messageList.filter((message) => {
		const { isPublic, recipient, transmitter } = message;
		return isPublic === false && (transmitter === userID || recipient === userID);
	});
	return messageResults;
};

/**
 * @function getUserByID
 * @param {Array.<{_id:String,firstName:String,lastName:String,pictureUrl:String}>} userList complete list of users
 * @param {string} userID specific user string id
 * @returns {{_id:String,firstName:String,lastName:String,pictureUrl:String}|null} return a user object or null
 */
const getUserByID = function (userList, userID) {
	if (!userList || !userID) return null;

	const searchResult = userList.filter((user) => user._id === userID);

	return searchResult.length === 1 ? searchResult[0] : null;
};

/**
 * @function getPrivateConversation
 * @param {Array.<{_id:String,transmitter:String,recipient:(String|null),isPublic:Boolean,date:object}>} messageList an array of all messages
 * @param {string} currentUserID the unique id of current user
 * @param {string} distantUserID the unique id of user that current user talk with
 * @returns {Array.<{_id:String,transmitter:String,recipient:String,isPublic:false,date:object}>} returns an array of private messages
 */
const getPrivateConversation = function (messageList, currentUserID, distantUserID) {
	if (!currentUserID || !distantUserID) return [];

	const searchResults = messageList.filter(
		(message) =>
			!message.isPublic &&
			(message.transmitter === currentUserID || message.transmitter === distantUserID) &&
			(message.recipient === currentUserID || message.recipient === distantUserID)
	);
	return searchResults;
};

/**
 * @function createPrivateMessage
 * @param {string} transmitterID sender user id
 * @param {string} recipientID destination user id
 * @param {string} content the message
 * @returns {{_id:String,transmitter:String,recipient:String,isPublic:false,date:object}}
 */
const createPrivateMessage = function (transmitterID, recipientID, content) {
	if (!transmitterID || !recipientID) {
		throw new Error(
			"transmitterID and recipientID must be passed to create a new private message"
		);
	}

	return {
		_id: uniqid("msg_"),
		date: Date(),
		content,
		transmitter: transmitterID,
		recipient: recipientID,
		isPublic: false,
	};
};

/**
 * @function createPublicMessage
 * @param {string} transmitterID sender user id
 * @param {string} content the message
 * @returns {{_id:String,transmitter:String,recipient:null,isPublic:true,date:object}}
 */
const createPublicMessage = function (transmitterID, content) {
	if (!transmitterID) {
		throw new Error("transmitterID must be passed to create a new private message");
	}

	return {
		_id: uniqid("msg_"),
		content,
		date: Date(),
		transmitter: transmitterID,
		recipient: null,
		isPublic: true,
	};
};

/**
 * @function getUserLastMessage
 * @param {Array.<{_id:String,transmitter:String,recipient:(String|null),isPublic:Boolean,date:object}>} messageList an array of all messages
 * @param {string} transmitterID author id
 */
const getUserLastMessage = function (messageList, transmitterID) {
	const messageFound = (el) => el.transmitter === transmitterID;
	const messageListSorted = messageList.sort((a, b) => new Date(a.date) - new Date(b.date));

	const messageID = messageListSorted.findIndex(messageFound);

	if (messageID >= 0) {
		return messageListSorted[messageID].content;
	}
	return ` ... `;
};

// EXPORTS API FUNCTIONS
export {
	getAllPublicMessages,
	getPrivateMessages,
	getUserByID,
	getUserLastMessage,
	getPrivateConversation,
	createPrivateMessage,
	createPublicMessage,
};
