// import libraires
import React from "react";
import ReactDOM from "react-dom";

// import components
import App from "./components/App";

// render application to client's browser
ReactDOM.render(
	<React.StrictMode>
		<App />
	</React.StrictMode>,
	document.querySelector("#root")
);
