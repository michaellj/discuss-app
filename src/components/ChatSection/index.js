// import libraries
import React from "react";

// import components
import Header from "../Header";
import Form from "../Form";
import ChatContent from "../ChatContent";

// create component
export default function ChatSection() {
	return (
		<section className='chat__section' data-test='chat-section-component'>
			<Header />

			<ChatContent />

			<Form />
		</section>
	);
}
