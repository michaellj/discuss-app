// import libraries
import { shallow } from "enzyme";

// import components
import ChatSection from "./";
import Header from "../Header";
import Form from "../Form";
import ChatContent from "../ChatContent";

// helpers & datas
import { checkFindElementByTestAttribute, checkFindComponent } from "../../../utils/tests";

/**
 * @function setup - Factory function to set the main JSX element to test for each suite
 * @returns {ShallowWrapper} Enzype shallow wrapper
 */
const setup = () => {
	return shallow(<ChatSection />);
};

// tests suites
describe("ChatSection", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup();
	});

	it("should render without errors", () => {
		checkFindElementByTestAttribute(wrapper, "chat-section-component");
	});

	it("should contain a Header component", () => {
		checkFindComponent(wrapper, Header);
	});

	it("should content a ChatContent component", () => {
		checkFindComponent(wrapper, ChatContent);
	});

	it("should contain a Form component", () => {
		checkFindComponent(wrapper, Form);
	});
});
