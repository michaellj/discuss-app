// import libraries
import { shallow } from "enzyme";

// import components
import UserSection from "./";
import UserList from "../UserList";
import MainUserRow from "../MainUserRow";
import ChatButton from "../ChatButton";

// datas & helpers
import { checkFindElementByTestAttribute, checkFindComponent } from "../../../utils/tests";

/**
 * @function setup - Factory function to set the main JSX element to test for each suite
 * @returns {ShallowWrapper} Enzype shallow wrapper
 */
const setup = () => {
	return shallow(<UserSection />);
};

// tests suites
describe("UserSection component structure", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup();
	});

	it("should render without errors", () => {
		checkFindElementByTestAttribute(wrapper, "user-section-component");
	});

	it("should contains a MainUserRow component", () => {
		checkFindComponent(wrapper, MainUserRow);
	});

	it("should contains a ChatButton component", () => {
		checkFindComponent(wrapper, ChatButton);
	});

	it("should contain a UserList component", () => {
		checkFindComponent(wrapper, UserList);
	});
});
