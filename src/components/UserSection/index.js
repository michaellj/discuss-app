// import libraries
import React from "react";

// import components
import MainUserRow from "../MainUserRow";
import ChatButton from "../ChatButton";
import UserList from "../UserList";

// create component
export default function UserSection() {
	return (
		<section className='user__section' data-test='user-section-component'>
			<MainUserRow />
			<ChatButton />
			<UserList />
		</section>
	);
}
