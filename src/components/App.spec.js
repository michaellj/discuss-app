// import libraries
import { shallow } from "enzyme";

// import components
import App from "./App";
import UserSection from "./UserSection";
import ChatSection from "./ChatSection";

// datas & helpers
import { checkFindElementByTestAttribute, checkFindComponent } from "../../utils/tests";

/**
 * @function setup - Factory function to set the main JSX element to test for each suite
 * @returns {ShallowWrapper} Enzype shallow wrapper
 */
const setup = () => {
	return shallow(<App />);
};

// tests suites
describe("App component structure", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup();
	});

	it("should render without errors", () => {
		checkFindElementByTestAttribute(wrapper, "app-component");
	});

	it("should contain a UserSection component", () => {
		checkFindComponent(wrapper, UserSection);
	});

	it("should contain a ChatSection component", () => {
		checkFindComponent(wrapper, ChatSection);
	});
});
