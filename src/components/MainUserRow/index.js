// import libraries
import React from "react";

// import components
import ProfileImage from "../ProfileImage";

// contexts
import userContext from "../../contexts/userContext";

// create component
export default function MainUserRow() {
	const user = userContext.useCurrentUser();
	const { firstName, lastName } = user;

	return (
		<div className='row user__main-row' data-test='main-row-component'>
			<div className='user__row-item'>
				<ProfileImage user={user} />

				<span className='user__text-section'>
					<p className='user__info'>Connecté en tant que :</p>
					<p className='user__name' data-test='user-fullname-element'>
						{firstName} {lastName}
					</p>
				</span>
			</div>
		</div>
	);
}
