// import libraries
import { shallow } from "enzyme";

// import components
import MainUserRow from "./";
import ProfileImage from "../ProfileImage";

// import contexts
import userContext from "../../contexts/userContext";

// helpers & datas
import { checkFindElementByTestAttribute, checkFindComponent } from "../../../utils/tests";

/**
 * Factory function to create a ShallowWrapper for the MainUserRow component.
 * @function setup
 * @param user - value specific to this setup
 * @returns {ShallowWrapper} Enzype shallow wrapper
 */
const setup = (user = {}) => {
	const mockUser = jest.fn().mockReturnValue(user);

	userContext.useCurrentUser = mockUser;
	return shallow(<MainUserRow />);
};

// tests suites
describe("MainUserRow component structure", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup();
	});

	it("should render without errors", () => {
		checkFindElementByTestAttribute(wrapper, "main-row-component");
	});

	it("should contain a ProfileImage component", () => {
		checkFindComponent(wrapper, ProfileImage);
	});

	it("should display user full name", () => {
		checkFindElementByTestAttribute(wrapper, "user-fullname-element");
	});
});
