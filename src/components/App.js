// import libraries
import React from "react";

// import components
import UserSection from "./UserSection";
import ChatSection from "./ChatSection";

// context
import userContext from "../contexts/userContext";
import chatPrivacyContext from "../contexts/chatPrivacyContext";
import messagesContext from "../contexts/messagesContext";

// create component
function App() {
	return (
		<main className='app__container' role='main' data-test='app-component'>
			<userContext.CurrentUserProvider>
				<userContext.UserListProvider>
					<userContext.SelectedUserProvider>
						<chatPrivacyContext.ChatPrivacyProvider>
							<messagesContext.MessagesProvider>
								<UserSection />

								<ChatSection />
							</messagesContext.MessagesProvider>
						</chatPrivacyContext.ChatPrivacyProvider>
					</userContext.SelectedUserProvider>
				</userContext.UserListProvider>
			</userContext.CurrentUserProvider>
		</main>
	);
}

// make component available
export default App;
