// import libraries
import { shallow } from "enzyme";

// import components
import Message from "./";

// helpers & datas
import {
	checkFindElementByTestAttribute,
	checkProps,
	findElementByTestAttribute,
} from "../../../utils/tests";

/**
 * @function setup - Factory function to set the main JSX element to test for each suite
 * @returns {ShallowWrapper} Enzype shallow wrapper
 */
const setup = () => {
	const message = {
		_id: "msg_4xjvy1926khepzlrv",
		transmitter: "usr-id0",
		recipient: "usr-4xjvy1926khepzlrf",
		isPublic: false,
		date: "2020-02-04T09:23:09.295Z",
		content: "Quis rerum dolor unde repellendus nulla eius dolorum rerum.",
	};
	return shallow(<Message message={message} currentUserID={"usr-id"} />);
};

// tests suites
describe("Message component structure", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup();
	});

	it("should render without error", () => {
		checkFindElementByTestAttribute(wrapper, "message-row-component");
	});

	it("should contain a header for message informations", () => {
		checkFindElementByTestAttribute(wrapper, "message-header");
	});

	it("should contain a content section for message content", () => {
		checkFindElementByTestAttribute(wrapper, "message-content");
	});

	it("should receive a `message` and a `user` props", () => {
		const conformingProps = {
			message: {
				_id: "msg_id",
				transmitter: "usr-idt",
				recipient: "usr-idr",
				isPublic: false,
				date: "2020-02-04T09:23:09.295Z",
				content: "Quis rerum dolor unde repellendus nulla eius dolorum rerum.",
			},
			currentUserID: "usr-id",
		};

		const propsError = checkProps(wrapper, conformingProps);
		expect(propsError).toBeUndefined();
	});

	it("should have a classname of `message__row` if current user is not the message writter", () => {
		wrapper.setProps({ currentUserID: "usr-id1" });
		expect(wrapper.hasClass("message__row")).toEqual(true);
	});

	it("should have a classname of `message__row--right` if current user is the writter of the message", () => {
		wrapper.setProps({ currentUserID: "usr-id0" });
		expect(wrapper.hasClass("message__row--right")).toEqual(true);
	});

	it('should display "Vous" if author `author` props is null', () => {
		wrapper.setProps({ author: null });
		const authorTextHTML = findElementByTestAttribute(wrapper, "message-header");

		expect(authorTextHTML.text().includes("Vous")).toBe(true);
	});

	it("should display author full name if author is not current user", () => {
		wrapper.setProps({ author: {} });
		const authorTextHTML = findElementByTestAttribute(wrapper, "message-header");

		expect(authorTextHTML.text().includes("Vous")).toBe(false);
	});
});
