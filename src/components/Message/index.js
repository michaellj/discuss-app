// import libraries
import React from "react";
import PropTypes from "prop-types";

// create components
function Message({ message, currentUserID, author }) {
	const { content, date } = message;
	const messageDate = new Date(date).toLocaleDateString("fr-fr", {
		year: "numeric",
		month: "long",
		day: "numeric",
		hour: "numeric",
		minute: "numeric",
	});

	let authorName;
	if (author) {
		authorName = `${author.firstName} ${author.lastName}`;
	} else {
		authorName = "Vous";
	}

	const messageRowClass =
		currentUserID === message.transmitter ? "message__row--right" : "message__row";

	return (
		<article className={messageRowClass} data-test='message-row-component'>
			<span className='message__container'>
				<p className='message__header' data-test='message-header'>
					{authorName} - le {messageDate}
				</p>
				<p className='message__content' data-test='message-content'>
					{content}
				</p>
			</span>
		</article>
	);
}

// props configuration
Message.defaultProps = {
	message: {
		_id: "msg_id",
		transmitter: "usr-idt",
		recipient: "usr-idr",
		isPublic: false,
		date: "2020-02-04T09:23:09.295Z",
		content: "Quis rerum dolor unde repellendus nulla eius dolorum rerum.",
	},
	currentUserID: "usr-id",
	author: null,
};

Message.propTypes = {
	message: PropTypes.shape({
		_id: PropTypes.string.isRequired,
		transmitter: PropTypes.string.isRequired,
		recipient: PropTypes.string,
		isPublic: PropTypes.bool.isRequired,
		date: PropTypes.string.isRequired,
		content: PropTypes.string.isRequired,
	}).isRequired,
	currentUserID: PropTypes.string.isRequired,
	author: PropTypes.shape({
		_id: PropTypes.string.isRequired,
		firstName: PropTypes.string.isRequired,
		lastName: PropTypes.string.isRequired,
		pictureUrl: PropTypes.string.isRequired,
	}),
};

// make component available
export default React.memo(Message);
