// import libraries
import { shallow } from "enzyme";

// import components
import UserList from "./";
import UserRow from "../UserRow";

import userContext from "../../contexts/userContext";
import chatPrivacyContext from "../../contexts/chatPrivacyContext";

// helpers & datas
import {
	checkFindElementByTestAttribute,
	findComponent,
	findElementByTestAttribute,
} from "../../../utils/tests";
import userListJSON from "../../datas/users.json";

/**
 * @function setup - Factory function to set the main JSX element to test for each suite
 * @returns {ShallowWrapper} Enzype shallow wrapper
 */
const setup = (users = [], chatPrivacy = false) => {
	const mockUserList = jest.fn().mockReturnValue(users);
	const mockChatPrivacy = jest.fn().mockReturnValue([chatPrivacy, jest.fn()]);
	userContext.useUserList = mockUserList;
	chatPrivacyContext.useChatPrivacy = mockChatPrivacy;
	return shallow(<UserList />);
};

// tests suites
describe("UserList component structure", () => {
	let wrapper;

	it("should render without errors", () => {
		wrapper = setup();
		checkFindElementByTestAttribute(wrapper, "user-list-component");
	});

	it("ishould not render any UserRow if there is no user", () => {
		wrapper = setup();
		const userRow = findComponent(wrapper, UserRow);
		expect(userRow).toHaveLength(0);
	});

	it("should render a UserRow component for each user", () => {
		wrapper = setup(userListJSON);
		const userRow = findComponent(wrapper, UserRow);
		expect(userRow).toHaveLength(userListJSON.length);
	});

	it("should have a className of `user__list` if chatPrivacy is false", () => {
		const userListComponent = findElementByTestAttribute(wrapper, "user-list-component");
		expect(userListComponent.hasClass("user__list")).toEqual(true);
	});

	it("should have a className of `user__list-short` if chatPrivacy is true", () => {
		wrapper = setup([], true);
		const userListComponent = findElementByTestAttribute(wrapper, "user-list-component");
		expect(userListComponent.hasClass("user__list-short")).toEqual(true);
	});
});
