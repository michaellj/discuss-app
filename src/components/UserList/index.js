// import libraries
import React from "react";

// import components
import UserRow from "../UserRow";

// import contexts
import userContext from "../../contexts/userContext";
import chatPrivacyContext from "../../contexts/chatPrivacyContext";

// create component
export default function UserList() {
	const users = userContext.useUserList();
	const [chatPrivacy] = chatPrivacyContext.useChatPrivacy();

	const content = users.map((user) => <UserRow key={user._id} user={user} />);

	const componentCssClass = chatPrivacy ? "user__list-short" : "user__list";

	return (
		<ul className={componentCssClass} data-test='user-list-component'>
			{content}
		</ul>
	);
}
