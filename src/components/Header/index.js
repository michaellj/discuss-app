// import libraries
import React from "react";

// import components
import ProfileImage from "../ProfileImage";

// context
import userContext from "../../contexts/userContext";
import chatPrivacyContext from "../../contexts/chatPrivacyContext";

// helpers
import { getUserByID } from "../../api";

// create component
export default function Header() {
	const [selectedUser] = userContext.useSelectedUser();
	const users = userContext.useUserList();
	const [chatPrivacy] = chatPrivacyContext.useChatPrivacy();

	if (chatPrivacy) {
		const user = getUserByID(users, selectedUser);

		return (
			<header className='chat__header u-border-bottom' data-test='chat-header-component'>
				<ProfileImage user={user} />
				<p className='chat__header-text' data-test='chat-header-text'>
					Conversation privée avec {user.firstName} {user.lastName}
				</p>
			</header>
		);
	}

	return (
		<header className='chat__header u-border-bottom' data-test='chat-header-component'>
			<p className='chat__header-text' data-test='chat-header-text'>
				Canal de Tchat général
			</p>
		</header>
	);
}
