// import libraries
import { shallow } from "enzyme";

// import components
import Header from ".";

// import contexts
import chatPrivacyContext from "../../contexts/chatPrivacyContext";
import userContext from "../../contexts/userContext";

// helpers & datas
import userListJSON from "../../datas/users.json";
import {
	checkFindComponent,
	checkFindElementByTestAttribute,
	findElementByTestAttribute,
} from "../../../utils/tests";
import ProfileImage from "../ProfileImage";

const setup = (users = [], chatPrivacy = false, selectedUser = "") => {
	const mockSelectedUser = jest.fn(() => [selectedUser, jest.fn()]);
	const mockChatPrivacy = jest.fn(() => [chatPrivacy, jest.fn()]);
	const mockUserList = jest.fn().mockReturnValue(users);
	userContext.useUserList = mockUserList;
	userContext.useSelectedUser = mockSelectedUser;
	chatPrivacyContext.useChatPrivacy = mockChatPrivacy;

	return shallow(<Header />);
};

// tests suites
describe("Header component structure", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup();
	});

	it("should render without errors", () => {
		checkFindElementByTestAttribute(wrapper, "chat-header-component");
	});

	it("should have a default text for public chat", () => {
		const textElement = findElementByTestAttribute(wrapper, "chat-header-text");
		expect(textElement.text()).toEqual("Canal de Tchat général");
	});

	it("should render a ProfileImage component with selectedUser informations if chatPrivacy is true", () => {
		const mockSelectedUser = userListJSON[0]._id;
		wrapper = setup(userListJSON, true, mockSelectedUser);
		checkFindComponent(wrapper, ProfileImage);
	});
});
