// import libraires
import React from "react";
import { shallow } from "enzyme";

// import components
import Form from ".";

// contexts
import userContext from "../../contexts/userContext";
import messagesContext from "../../contexts/messagesContext";
import chatPrivacyContext from "../../contexts/chatPrivacyContext";

// helpers & datas
import { checkFindElementByTestAttribute, findElementByTestAttribute } from "../../../utils/tests";
import * as api from "../../api";

jest.mock("../../api");

const mockSetMessages = jest.fn();

/**
 * Factory function to create a ShallowWrapper for the MainUserRow component.
 * @function setup
 * @param user - value specific to this setup
 * @returns {ShallowWrapper} Enzype shallow wrapper
 */
const setup = (messages = [], user = {}, selectedUser = "", chatPrivacy = false) => {
	const mockCurrentUser = jest.fn().mockReturnValue(user);
	const mockMessages = jest.fn().mockReturnValue([messages, mockSetMessages]);
	const mockSelectedUser = jest.fn().mockReturnValue([selectedUser, jest.fn()]);
	const mockChatPrivacy = jest.fn().mockReturnValue([chatPrivacy, jest.fn()]);

	userContext.useCurrentUser = mockCurrentUser;
	userContext.useSelectedUser = mockSelectedUser;
	messagesContext.useMessages = mockMessages;
	chatPrivacyContext.useChatPrivacy = mockChatPrivacy;
	return shallow(<Form />);
};

// tests suites
describe("Form component structure", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup();
	});

	it("should render without errors", () => {
		checkFindElementByTestAttribute(wrapper, "chat-form-component");
	});

	it("should contain an input", () => {
		const formInput = wrapper.find("input");
		expect(formInput.length).toEqual(1);
	});
});

describe("Input state controlled field", () => {
	let mockSetMessageContent = jest.fn();
	let wrapper;

	beforeEach(() => {
		mockSetMessageContent.mockClear();
		React.useState = jest.fn(() => ["", mockSetMessageContent]);
		wrapper = setup();
	});

	afterEach(() => {
		jest.clearAllMocks();
	});

	it("should update state with value of input box upon change", () => {
		const inputBox = findElementByTestAttribute(wrapper, "form-input-element");
		const mockEvent = { target: { name: "message-input", value: "Hello test message" } };
		inputBox.simulate("change", mockEvent);

		expect(mockSetMessageContent).toHaveBeenCalledWith("Hello test message");
	});

	it("should clear the current message content upon form submit", () => {
		wrapper.simulate("submit", { preventDefault() {} });
		expect(api.createPublicMessage).toHaveBeenCalled();
		expect(mockSetMessageContent).toHaveBeenCalledWith("");
		expect(mockSetMessages).toHaveBeenCalled();
	});

	it("should create private message if chatPrivacy is true", () => {
		wrapper = setup([], { _id: "userID" }, "selectedUserID", true);
		wrapper.simulate("submit", { preventDefault() {} });
		expect(api.createPrivateMessage).toHaveBeenCalled();
	});
});
