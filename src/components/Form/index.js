// import libraries
import React from "react";

// import contexts
import messagesContext from "../../contexts/messagesContext";
import userContext from "../../contexts/userContext";
import chatPrivacyContext from "../../contexts/chatPrivacyContext";

// helpers
import { createPublicMessage, createPrivateMessage } from "../../api";

// create component
export default function Form() {
	const currentUser = userContext.useCurrentUser();
	const [messages, setMessages] = messagesContext.useMessages();
	const [selectedUser] = userContext.useSelectedUser();
	const [chatPrivacy] = chatPrivacyContext.useChatPrivacy();

	const [messageContent, setMessageContent] = React.useState("");

	const onFormSubmit = (e) => {
		e.preventDefault();

		let newMessage;
		if (chatPrivacy) {
			newMessage = createPrivateMessage(currentUser._id, selectedUser, messageContent);
		} else {
			newMessage = createPublicMessage(currentUser._id, messageContent);
		}
		setMessages((messages) => [...messages, newMessage]);

		setMessageContent("");
	};

	return (
		<form className='chat__footer' data-test='chat-form-component' onSubmit={onFormSubmit}>
			<input
				name='message-input'
				type='text'
				className='chat__input'
				placeholder='Taper un message'
				value={messageContent}
				onChange={(e) => setMessageContent(e.target.value)}
				data-test='form-input-element'
			/>
		</form>
	);
}
