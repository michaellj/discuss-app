// import libraries
import { shallow } from "enzyme";

// import components
import ChatContent from "./";
import Message from "../Message";

import messagesContext from "../../contexts/messagesContext";
import userContext from "../../contexts/userContext";
import chatPrivacyContext from "../../contexts/chatPrivacyContext";

// datas & helpers
import { checkFindElementByTestAttribute, findComponent } from "../../../utils/tests";
import messagesListJSON from "../../datas/messages.json";
import userListJSON from "../../datas/users.json";
import { getAllPublicMessages, getPrivateConversation } from "../../api";

/**
 * @function setup - Factory function to set the main JSX element to test for each suite
 * @returns {ShallowWrapper} Enzype shallow wrapper
 */
const setup = (messages = [], user = {}, userList = [], selectedUser = "", chatPrivacy = false) => {
	const mockSelectedUser = jest.fn(() => [selectedUser, jest.fn()]);
	const mockMessages = jest.fn().mockReturnValue([messages, jest.fn()]);
	const mockUser = jest.fn().mockReturnValue(user);
	const mockUserList = jest.fn().mockReturnValue(userList);
	const mockChatPrivacy = jest.fn(() => [chatPrivacy, jest.fn()]);
	messagesContext.useMessages = mockMessages;
	userContext.useCurrentUser = mockUser;
	userContext.useUserList = mockUserList;
	userContext.useSelectedUser = mockSelectedUser;
	chatPrivacyContext.useChatPrivacy = mockChatPrivacy;
	return shallow(<ChatContent />);
};

// suite test
describe("ChatContent component structure", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup();
	});
	it("should render without errors", () => {
		checkFindElementByTestAttribute(wrapper, "chat-content-component");
	});

	it("should not render any Message component if there is no messages", () => {
		const messageRow = findComponent(wrapper, Message);
		expect(messageRow).toHaveLength(0);
	});

	it("should render one Message component for each message", () => {
		wrapper = setup(messagesListJSON);
		const publicMessages = getAllPublicMessages(messagesListJSON);
		const messageRow = findComponent(wrapper, Message);
		expect(messageRow).toHaveLength(publicMessages.length);
	});
});

describe("Private ChatContent", () => {
	it("should render private conversation", () => {
		const currentUser = userListJSON[0];
		const selectedUser = userListJSON[1]._id;
		const wrapper = setup(messagesListJSON, currentUser, userListJSON, selectedUser, true);
		const privateMessages = getPrivateConversation(
			messagesListJSON,
			currentUser._id,
			selectedUser
		);
		const messageRow = findComponent(wrapper, Message);
		expect(messageRow).toHaveLength(privateMessages.length);
	});
});
