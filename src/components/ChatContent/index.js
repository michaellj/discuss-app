// import libraries
import React from "react";

// import components
import Message from "../Message";

// contexts
import messagesContext from "../../contexts/messagesContext";
import userContext from "../../contexts/userContext";
import chatPrivacyContext from "../../contexts/chatPrivacyContext";

// helpers
import { getAllPublicMessages, getUserByID, getPrivateConversation } from "../../api";

// create component
export default function ChatContent() {
	const [chatPrivacy] = chatPrivacyContext.useChatPrivacy();
	const [messages] = messagesContext.useMessages();
	const currentUser = userContext.useCurrentUser();
	const userList = userContext.useUserList();
	const [selectedUser] = userContext.useSelectedUser();

	const bottomContent = React.useRef(null);

	let currentMessageList;

	if (!chatPrivacy) {
		currentMessageList = getAllPublicMessages(messages);
	} else {
		currentMessageList = getPrivateConversation(messages, currentUser._id, selectedUser);
	}

	const content = currentMessageList.map((message) => {
		const author = getUserByID(userList, message.transmitter);
		return (
			<Message
				key={message._id}
				message={message}
				currentUserID={currentUser._id}
				author={author}
			/>
		);
	});

	React.useEffect(() => {
		bottomContent.current.scrollIntoView({ behavior: "auto" });
	});

	return (
		<div className='chat__content' data-test='chat-content-component'>
			{content}
			<div ref={bottomContent}></div>
		</div>
	);
}
