// import librarie
import React from "react";
import PropTypes from "prop-types";

// create component
function ProfileImage({ user }) {
	const { firstName, lastName } = user;
	const imageSrc = `https://identicon-api.herokuapp.com/${firstName}${lastName}/48?format=png`;
	const userFullName = firstName + lastName;
	return (
		<span className='user__picture-container' data-test='profile-image-component'>
			<figure className='user__picture-circle'>
				<img src={imageSrc} alt={userFullName} />
			</figure>
		</span>
	);
}

// props configuration
ProfileImage.defaultProps = {
	user: {
		firstName: "John",
		lastName: "Doe",
		pictureUrl: "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?f=y",
	},
};

ProfileImage.propTypes = {
	user: PropTypes.shape({
		firstName: PropTypes.string.isRequired,
		lastName: PropTypes.string.isRequired,
		pictureUrl: PropTypes.string.isRequired,
	}).isRequired,
};

// make component available
export default React.memo(ProfileImage);
