// import libraries
import { shallow } from "enzyme";

// import components
import ProfileImage from "./";

// helpers & datas
import { checkFindElementByTestAttribute, checkProps } from "../../../utils/tests";

const defaultProps = {
	_id: "id",
	firstName: "fname",
	lastName: "lname",
	pictureUrl: "pics",
};

/**
 * @function setup - Factory function to set the main JSX element to test for each suite
 * @returns {ShallowWrapper} Enzype shallow wrapper
 */
const setup = () => {
	const mockUser = {
		_id: "mock_id",
		firstName: "mock_fname",
		lastName: "mock_lname",
		pictureUrl: "mock_url",
	};
	return shallow(<ProfileImage user={mockUser} />);
};

// tests suites
describe("ProfileImage component structure", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup();
	});

	it("should render without errors", () => {
		checkFindElementByTestAttribute(wrapper, "profile-image-component");
	});

	it("should contain a `figure` html element", () => {
		const figureElement = wrapper.find("figure");
		expect(figureElement.length).toEqual(1);
	});

	it("should contain a `img` html element", () => {
		const imgElement = wrapper.find("img");
		expect(imgElement.length).toEqual(1);
	});

	it("should receive a user object as props", () => {
		checkProps(wrapper, defaultProps);
	});
});
