// import libraries
import React from "react";
import PropTypes from "prop-types";

// import components
import ProfileImage from "../ProfileImage";

// contexts
import userContext from "../../contexts/userContext";
import chatPrivacyContext from "../../contexts/chatPrivacyContext";
import messageContexts from "../../contexts/messagesContext";

// helpers
import { getUserLastMessage } from "../../api";

// create component
function UserRow({ user }) {
	const { firstName, lastName } = user;
	const [chatPrivacy, setChatPrivacy] = chatPrivacyContext.useChatPrivacy();
	const [selectedUser, setSelectedUser] = userContext.useSelectedUser();
	const [messages] = messageContexts.useMessages();

	const handleSelectUser = () => {
		setSelectedUser(user._id);
		setChatPrivacy(true);
	};

	const userLastmessage = getUserLastMessage(messages, user._id);

	return (
		<li className='row user__row' data-test='user-row-component'>
			<button
				className='user__row-item'
				data-test='default-row-button'
				onClick={handleSelectUser}>
				<ProfileImage user={user} />

				<span className='user__text-section'>
					<p className='user__name' data-test='user-fullname-element'>
						{firstName} {lastName}
					</p>
					<p className='user__info'>{userLastmessage}</p>
				</span>
			</button>
		</li>
	);
}

// props settings

UserRow.defaultProps = {
	user: {
		_id: "user_id",
		firstName: "John",
		lastName: "Doe",
		pictureUrl: "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?f=y",
	},
};

UserRow.propTypes = {
	user: PropTypes.shape({
		_id: PropTypes.string.isRequired,
		firstName: PropTypes.string.isRequired,
		lastName: PropTypes.string.isRequired,
		pictureUrl: PropTypes.string.isRequired,
	}).isRequired,
};

// make component available
export default React.memo(UserRow);
