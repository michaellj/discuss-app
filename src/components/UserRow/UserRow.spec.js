// import libraries
import { shallow } from "enzyme";

// import components
import UserRow from "./";
import ProfileImage from "../ProfileImage";

// import contexts
import userContext from "../../contexts/userContext";
import chatPrivacyContext from "../../contexts/chatPrivacyContext";
import messagesContext from "../../contexts/messagesContext";

// helpers & datas
import {
	checkFindComponent,
	checkFindElementByTestAttribute,
	findElementByTestAttribute,
	checkProps,
} from "../../../utils/tests";

const mockSetSelectedUser = jest.fn();
const mockSetChatPrivacy = jest.fn();

/**
 * @function setup - Factory function to set the main JSX element to test for each suite
 * @returns {ShallowWrapper} Enzype shallow wrapper
 */
const setup = (selectedUser = {}, chatPrivacy = false, messages = []) => {
	const mockSelectedUser = jest.fn(() => [selectedUser, mockSetSelectedUser]);
	const mockChatPrivacy = jest.fn(() => [chatPrivacy, mockSetChatPrivacy]);
	const mockMessages = jest.fn(() => [messages, jest.fn()]);

	userContext.useSelectedUser = mockSelectedUser;
	chatPrivacyContext.useChatPrivacy = mockChatPrivacy;
	messagesContext.useMessages = mockMessages;

	const user = {
		_id: "user_id",
		firstName: "John",
		lastName: "Doe",
		pictureUrl: "web_url",
	};

	return shallow(<UserRow user={user} />);
};

// test suites
describe("UserRow component structure", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup();
	});

	it("should render without errors", () => {
		checkFindElementByTestAttribute(wrapper, "user-row-component");
	});

	it("should contain a `button` html element", () => {
		checkFindElementByTestAttribute(wrapper, "default-row-button");
	});

	it("should contain a ProfileImage component", () => {
		checkFindComponent(wrapper, ProfileImage);
	});

	it("should display user fullname", () => {
		checkFindElementByTestAttribute(wrapper, "user-fullname-element");
	});

	it("should receive a user object props", () => {
		const defaultProps = {
			_id: "id",
			firstName: "firstName",
			lastName: "lastName",
			pictureUrl: "pictureUrl",
		};

		const propsError = checkProps(wrapper, defaultProps);
		expect(propsError).toBeUndefined();
	});

	it("should render user full name", () => {
		const textElement = findElementByTestAttribute(wrapper, "user-fullname-element");
		expect(textElement.text()).toEqual("John Doe");
	});

	it("should update selectedUser and chatPrivacy when clicked", () => {
		const rowButton = findElementByTestAttribute(wrapper, "default-row-button");

		rowButton.simulate("click");

		expect(mockSetChatPrivacy).toHaveBeenCalled();
		expect(mockSetSelectedUser).toHaveBeenCalled();
	});
});
