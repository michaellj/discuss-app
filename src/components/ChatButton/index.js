// import libraries
import React from "react";

// contexts
import chatPrivacyContext from "../../contexts/chatPrivacyContext";
import userContext from "../../contexts/userContext";

// create component
export default function ChatButton() {
	const [chatPrivacy, setChatPrivacy] = chatPrivacyContext.useChatPrivacy();
	const [selectedUser, setSelectedUser] = userContext.useSelectedUser();

	const isButtonVisible = chatPrivacy ? "flex" : "none";

	const handleChatPrivacy = () => {
		setChatPrivacy(false);
		setSelectedUser("");
	};

	return (
		<div
			className='row u-border-bottom u-block-flex u-block-center-items'
			data-test='chat-button-component'
			style={{ display: isButtonVisible }}>
			<button
				className='btn btn-primary'
				data-test='button-component'
				onClick={handleChatPrivacy}>
				Retour à la conversation générale
			</button>
		</div>
	);
}
