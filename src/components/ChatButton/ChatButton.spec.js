// import libraries
import { shallow } from "enzyme";

// import components
import ChatButton from "./";
// import Button from "../Button";

// import contexts
import chatPrivacyContext from "../../contexts/chatPrivacyContext";
import userContext from "../../contexts/userContext";

// helpers & datas
import { checkFindElementByTestAttribute, findElementByTestAttribute } from "../../../utils/tests";

const mockSetSelectedUser = jest.fn();
const mockSetChatPrivacy = jest.fn();

/**
 * @function setup - Factory function to set the main JSX element to test for each suite
 * @returns {ShallowWrapper} Enzype shallow wrapper
 */
const setup = (chatPrivacy = false, selectedUser = "") => {
	const mockSelectedUser = jest.fn(() => [selectedUser, mockSetSelectedUser]);
	const mockChatPrivacy = jest.fn(() => [chatPrivacy, mockSetChatPrivacy]);
	userContext.useSelectedUser = mockSelectedUser;
	chatPrivacyContext.useChatPrivacy = mockChatPrivacy;
	return shallow(<ChatButton />);
};

// tests suites
describe("ChatButton component structure", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup();
	});

	it("should render without error", () => {
		checkFindElementByTestAttribute(wrapper, "chat-button-component");
	});

	it("should render a html button component", () => {
		checkFindElementByTestAttribute(wrapper, "button-component");
	});

	it("should be hidden when chatPrivacy is true", () => {
		const chatButton = findElementByTestAttribute(wrapper, "chat-button-component");
		expect(chatButton.get(0).props.style.display).toEqual("none");
	});

	it("should be be visible when chatPrivacy is false", () => {
		wrapper = setup(true, "userID");
		const chatButton = findElementByTestAttribute(wrapper, "chat-button-component");
		expect(chatButton.get(0).props.style.display).toEqual("flex");
	});

	it("should reset chatPrivacy and selectedUser state to default on click", () => {
		const btn = findElementByTestAttribute(wrapper, "button-component");
		btn.simulate("click");
		expect(mockSetSelectedUser).toHaveBeenCalled();
		expect(mockSetChatPrivacy).toHaveBeenCalled();
	});
});
