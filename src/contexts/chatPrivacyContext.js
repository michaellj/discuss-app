// import libraries
import React from "react";

// context
const chatPrivacyContext = React.createContext();

/**
 * @function useChatPrivacy - custom hook
 * @returns {array} successContext value, which is a state of [value, setter].
 */
export function useChatPrivacy() {
	const context = React.useContext(chatPrivacyContext);
	if (!context) throw new Error("useChatPrivacy must be used within a ChatPrivacyProvider");
	return context;
}

/**
 * @function ChatPrivacyProvider
 * @param {object} props - props to pass through from declared component
 * @returns {JSX.Element} Provider component
 */
export function ChatPrivacyProvider(props) {
	const [chatPrivacy, setChatPrivacy] = React.useState(false);

	const value = React.useMemo(() => [chatPrivacy, setChatPrivacy], [chatPrivacy]);

	return <chatPrivacyContext.Provider value={value} {...props} />;
}

const _chatPrivacyContext = { useChatPrivacy, ChatPrivacyProvider };

export default _chatPrivacyContext;
