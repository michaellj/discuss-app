// import libraries
import React from "react";
import { shallow, mount } from "enzyme";

// import context
import messagesContext from "./messagesContext";

// a functional component that calls use for our tests
const FunctionalComponent = () => {
	messagesContext.useMessages();
	return <div />;
};

// tests suites
describe("messagesContext", () => {
	it("should throw Error if is not wrapped in MessagesProvider", () => {
		expect(() => {
			shallow(<FunctionalComponent />);
		}).toThrowError("useMessages must be used within MessagesProvider");
	});
	it("should not throw Error if is wrapped in MessagesProvider", () => {
		expect(() => {
			mount(
				<messagesContext.MessagesProvider>
					<FunctionalComponent />
				</messagesContext.MessagesProvider>
			);
		}).not.toThrow();
	});
});
