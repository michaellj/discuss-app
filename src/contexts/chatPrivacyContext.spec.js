// import libraries
import React from "react";
import { shallow, mount } from "enzyme";

// import context
import chatPrivacyContext from "./chatPrivacyContext";

// a functional component that calls use for our tests
const FunctionalComponent = () => {
	chatPrivacyContext.useChatPrivacy();
	return <div />;
};

// tests suites
describe("chatPrivacyContext", () => {
	it("should throw error when chatPrivacy is not wrapped in ChatPrivacyProvider", () => {
		expect(() => {
			shallow(<FunctionalComponent />);
		}).toThrowError("useChatPrivacy must be used within a ChatPrivacyProvider");
	});

	it("should not throw error when useChatPrivacy is wrapper in ChatPrivacyProvider", () => {
		expect(() => {
			mount(
				<chatPrivacyContext.ChatPrivacyProvider>
					<FunctionalComponent />
				</chatPrivacyContext.ChatPrivacyProvider>
			);
		}).not.toThrow();
	});
});
