// import libraries
import React from "react";

// import datas
import usersJSON from "../datas/users.json";

const _currentUser = usersJSON[5];
const _userList = usersJSON.filter((user) => user._id !== _currentUser._id);

// create context
const currentUserContext = React.createContext();
const userListContext = React.createContext();
const selectedUserContext = React.createContext();

/**
 * @function useCurrentUser
 * @returns {array} currentUserContext value
 */
export function useCurrentUser() {
	const context = React.useContext(currentUserContext);

	if (!context) throw new Error("useUser must be used within a UserContextProvider");

	return context;
}

/**
 * @function CurrentUserProvider
 * @param {object} props - props to pass through from declared component
 * @returns {JSX.Element} Provider component
 */
export function CurrentUserProvider(props) {
	return <currentUserContext.Provider value={_currentUser} {...props} />;
}

/**
 * @function useUserList
 * @returns {array} userListContext value
 */
export function useUserList() {
	const context = React.useContext(userListContext);

	if (!context) throw new Error("userListContext must be used within a UserListProvider");

	return context;
}

export function UserListProvider(props) {
	return <userListContext.Provider value={_userList} {...props} />;
}

/**
 * @function useSelectedUser
 * @returns {array} currentUserContext value
 */
export function useSelectedUser() {
	const context = React.useContext(selectedUserContext);

	if (!context) throw new Error("useSelectedUser must be used within a SelectedUserProvider");

	return context;
}

/**
 * @function SelectedUserProvider
 * @param {object} props - props to pass through from declared component
 * @returns {JSX.Element} Provider component
 */
export function SelectedUserProvider(props) {
	const [selectedUser, setSelectedUser] = React.useState(null);

	const value = React.useMemo(() => [selectedUser, setSelectedUser], [selectedUser]);

	return <selectedUserContext.Provider value={value} {...props} />;
}

const userContext = {
	useCurrentUser,
	CurrentUserProvider,
	useUserList,
	UserListProvider,
	useSelectedUser,
	SelectedUserProvider,
};

export default userContext;
