// import libraries
import React from "react";
import { shallow, mount } from "enzyme";

// import contexts
import {
	useCurrentUser,
	CurrentUserProvider,
	useUserList,
	UserListProvider,
	useSelectedUser,
	SelectedUserProvider,
} from "./userContext";

const UserFunctionalComponent = () => {
	useCurrentUser();
	return <div />;
};

const UserListFunctionalComponent = () => {
	useUserList();
	return <div />;
};

const SelectedUserComponent = () => {
	useSelectedUser();
	return <div />;
};

describe("currentUserContext", () => {
	it("should throw error when useGuessedWords is not wrapped in CurrentUserProvider", () => {
		expect(() => {
			shallow(<UserFunctionalComponent />);
		}).toThrow("useUser must be used within a UserContextProvider");
	});

	it("should not throw error when useGuessedWords is wrapped in CurrentUserProvider", () => {
		expect(() => {
			mount(
				<CurrentUserProvider>
					<UserFunctionalComponent />
				</CurrentUserProvider>
			);
		}).not.toThrow();
	});
});

describe("userListContext", () => {
	it("should throw error when useGuessedWords is not wrapped in UserListProvider", () => {
		expect(() => {
			shallow(<UserListFunctionalComponent />);
		}).toThrow("userListContext must be used within a UserListProvider");
	});

	it("should not throw error when useGuessedWords is wrapped in UserListProvider", () => {
		expect(() => {
			mount(
				<UserListProvider>
					<UserListFunctionalComponent />
				</UserListProvider>
			);
		}).not.toThrow();
	});
});

describe("selectedUserContext", () => {
	it("should throw error when useGuessedWords is not wrapped in CurrentUserProvider", () => {
		expect(() => {
			shallow(<SelectedUserComponent />);
		}).toThrow("useSelectedUser must be used within a SelectedUserProvider");
	});

	it("should not throw error when useGuessedWords is wrapped in CurrentUserProvider", () => {
		expect(() => {
			mount(
				<SelectedUserProvider>
					<SelectedUserComponent />
				</SelectedUserProvider>
			);
		}).not.toThrow();
	});
});
