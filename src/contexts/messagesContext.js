// import libraires
import React from "react";

// datas
import messagesJSON from "../datas/messages.json";

// context
const messagesContext = React.createContext();

/**
 * @function useChatPrivacy - custom hook
 * @returns {array} successContext value, which is a state of [value, setter].
 */
export function useMessages() {
	const context = React.useContext(messagesContext);
	if (!context) throw new Error("useMessages must be used within MessagesProvider");

	return context;
}

/**
 * @function MessagesProvider
 * @param {object} props - props to pass through from declared component
 * @returns {JSX.Element} Provider component
 */
export function MessagesProvider(props) {
	const [messages, setMessages] = React.useState(messagesJSON);

	const value = React.useMemo(() => [messages, setMessages], [messages]);

	return <messagesContext.Provider value={value} {...props} />;
}

const _messagesContext = { useMessages, MessagesProvider };
export default _messagesContext;
