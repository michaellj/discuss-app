// import libraries
import checkPropTypes from "check-prop-types";

// create helper functions

/**
 * @function checkFindElementByTestAttribute
 * @param {ShallowWrapper} wrapper
 * Enzyme shallow wrapper
 * @param {String} attribute
 * html `data-test` element attribute value
 */
export function checkFindElementByTestAttribute(wrapper, attribute) {
	return expect(wrapper.find(`[data-test="${attribute}"]`)).toHaveLength(1);
}

/**
 * @function findElementByTestAttribute
 * @param {ShallowWrapper} wrapper
 * Enzyme shallow wrapper
 * @param {String} attribute
 * html `data-test` element attribute value
 * @returns {Enzyme.ShallowWrapper} Enzyme Wrapper
 */
export function findElementByTestAttribute(wrapper, attribute) {
	return wrapper.find(`[data-test="${attribute}"]`);
}

/**
 *
 * @param {ShallowWrapper} wrapper
 * Enzyme shallow wrapper
 * @param {JSX.Element} component
 * imported React Component
 */
export function checkFindComponent(wrapper, component) {
	return expect(wrapper.find(component)).toHaveLength(1);
}

/**
 *
 * @param {ShallowWrapper} wrapper
 * Enzyme shallow wrapper
 * @param {JSX.Element} component
 * imported React Component
 *  @returns {Enzyme.ShallowWrapper} Enzyme Wrapper
 */
export function findComponent(wrapper, component) {
	return wrapper.find(component);
}

/**
 * @function checkProps
 * Helper function using check-prop-types module for React testing components
 * @param {JSX.Element} component
 * The React component to check
 * @param {object} conformingProps
 * The props you need to check
 */
export function checkProps(component, conformingProps) {
	const propError = checkPropTypes(component.propTypes, conformingProps, "prop", component.name);
	expect(propError).toBeUndefined();
}
