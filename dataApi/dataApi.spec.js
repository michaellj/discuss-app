const fs = jest.createMockFromModule("fs");
const api = require("./dataApi");

const {
	_getRandomNumber,
	_getRandomBoolean,
	createFolder,
	createUserList,
	createMessageList,
} = api;

describe("API helpers functions", () => {
	describe("_getRandomNumber", () => {
		it("should throw an error if numMax is not a number", () => {
			expect(() => _getRandomNumber("test function")).toThrow();
		});
		it("should return a number which is less than or equal to numMax", () => {
			const value = 5;
			const result = _getRandomNumber(value);
			expect(result).toBeLessThanOrEqual(value);
		});
	});
	describe("_getRandomBoolean", () => {
		it("should return a boolean", () => {
			const value = _getRandomBoolean();
			expect(value === true || value === false).toBe(true);
		});
	});
});

describe("API datas functions", () => {
	describe("createFolder", () => {
		it("should create a folder for users and messages json files.", () => {
			createFolder("test", __dirname, fs.mkdirSync);
			expect(fs.mkdirSync).toHaveBeenCalledTimes(1);
			fs.mkdirSync.mockRestore();
		});
	});
	describe("createUserList", () => {
		it("should throw an error if needed argument are not passed", () => {
			expect(() => createUserList()).toThrow();
		});

		it("should call callback if everything it is ok", () => {
			createUserList(1, __dirname, "test", "test", fs.writeFileSync);
			expect(fs.writeFileSync).toHaveBeenCalledTimes(1);
			fs.writeFileSync.mockRestore();
		});
	});
	describe("createMessageList", () => {
		it("should throw an error if needed argument are not passed", () => {
			expect(() => createMessageList()).toThrow();
		});

		it("should call callback if everything it is ok", () => {
			createUserList(1, __dirname, "test", "testMessage", fs.writeFileSync);
			expect(fs.writeFileSync).toHaveBeenCalledTimes(1);
			fs.writeFileSync.mockRestore();
		});
	});
});
