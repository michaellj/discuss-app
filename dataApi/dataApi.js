// import libraries
const fs = require("fs");
const faker = require("faker");

// import models
const createUser = require("./models/user");
const createMessage = require("./models/message");

/**
 * @function _getRandomNumber
 * Simple helper function wich return a random number between 0 and the max number passed as argument
 * @param {Number} numMax
 * Should be an integer number
 * @returns {Number}
 * returns an integer number
 */
function _getRandomNumber(numMax = 1) {
	if (isNaN(numMax)) throw new Error("numMax should be a number");
	numMax = Math.floor(numMax);
	return Math.floor(Math.random() * (numMax - 0)) + 0;
}

/**
 * @function _getRandomBoolean
 * Simple helper function wich return true or false
 * @returns {Boolean}
 * Returns true or false
 */
function _getRandomBoolean() {
	return Math.random() > 0.5 ? true : false;
}

/**
 * @function createFolder
 * Create a specific folder with a path and its name
 * @param {String} folderName
 * The name you want for your datas
 * @param {String} path
 * The path where to store your datas
 * @param {Function} callback
 * [Optional] - a function used to create a folder.
 * By default it uses fs.mkdirSync node.js FileSystem method
 */
function createFolder(folderName, path, callback = fs.mkdirSync) {
	callback(`${path}/${folderName}`, { recursive: true });
}

/**
 * @function createUserList
 * Create a random users list and write it in a file in a specific folder
 * @param {Number} amount
 * Sould be an integer - The quantity of users you need
 * @param {String} path
 * The root where your file's folder is
 * @param {String} folderName
 * The folder name where your file should be saved
 * @param {String} fileName
 * The name you want to give for your file
 * @param {Function} callback
 * [Optional] - A function used to create a file.
 * By default it uses fs.writeFileSync nodes.js FileSystem method
 */
function createUserList(amount, path, folderName, fileName, callback = fs.writeFileSync) {
	if (!amount || !path || !folderName || !fileName) {
		throw new Error("All arguments are needed to create a user list !");
	}

	console.log(`API will generate ${amount} new users`);

	const userList = [];

	for (let i = 0; i < amount; i++) {
		userList.push(createUser());
	}

	callback(
		`${path}/${folderName}/${fileName}.json`,
		JSON.stringify(userList),
		{ flag: "w+" },
		(err) => {
			if (err) throw err;
		}
	);

	console.log(`${amount} users created in ${path}/${folderName}/${fileName}.json`);
}

/**
 *
 * @param {Number} amount
 * Sould be an integer - The quantity of messages you need
 * @param {Array.<{_id: String,firstName: String, lastName: String, pictureUrl: String}>} userList
 * An array of objects containing generated users
 * @param {String} path
 * The root where your file's folder is
 * @param {String} folderName
 * The folder name where your file should be saved
 * @param {String} fileName
 * The name you want to give for your file
 * @param {Function} callback
 * [Optional] - A function used to create a file.
 * By default it uses fs.writeFileSync nodes.js FileSystem method
 */
function createMessageList(
	amount,
	userList,
	path,
	folderName,
	fileName,
	idName,
	callback = fs.writeFileSync
) {
	if (!amount || !userList || !path || !folderName || !fileName || !idName) {
		throw new Error("All arguments are needed to create a messages list !");
	}

	console.log(`API will generate ${amount} fake messages including private and public`);

	const userLength = userList.length;
	const messageList = [];

	for (let i = 0; i < amount; i++) {
		const isPublic = _getRandomBoolean();
		let message;

		if (isPublic) {
			message = createMessage.publicMessage(
				userList[_getRandomNumber(userLength)][idName],
				faker.lorem.sentence()
			);
		} else {
			const userIdOne = userList[_getRandomNumber(userLength)][idName];
			let userIdTwo = userList[_getRandomNumber(userLength)][idName];

			if (userIdOne !== userIdTwo) {
				message = createMessage.privateMessage(
					userIdOne,
					userIdTwo,
					faker.lorem.sentence()
				);
			} else {
				userIdTwo = userList[_getRandomNumber(userLength)][idName];
				message = createMessage.privateMessage(
					userIdOne,
					userIdTwo,
					faker.lorem.sentence()
				);
			}
		}
		messageList.push(message);
	}

	callback(
		`${path}/${folderName}/${fileName}.json`,
		JSON.stringify(messageList),
		{ flag: "w+" },
		(err) => {
			if (err) throw err;
		}
	);

	console.log(`${amount} message have been created in ${path}/${folderName}/${fileName}.json`);
}

/**
 * @function generateDatas
 * Create fake datas with users and messages from them to test your application
 * @param {string} folderName
 * Name of the folder to create and where to store your datas
 * @param {Number} usersAmount
 * The quantity of users you want to create
 * @param {Number} messagesAmount
 * The quantity of message you want to create from your users
 * @param {String} rootFolder
 * The root path of your future folder
 * @param {Array.<[userFileName: String, messageFileName: String]>} fileNames
 * An array wich take the two file names without there extensions. Eg: ["users", "messages"]
 * @param {String} idName
 * The name of the id used to create messages with relationship between users
 * @param {Function} folderCallback
 * [Optional] - a function used to create a folder.
 * By default it uses fs.mkdirSync node.js FileSystem method
 * @param {Function} fileCallback
 * [Optional] - A function used to create a file.
 * By default it uses fs.writeFileSync nodes.js FileSystem method
 */

function generateDatas(
	folderName,
	usersAmount,
	messagesAmount,
	rootFolder,
	fileNames,
	idName,
	folderCallback = fs.mkdirSync,
	fileCallback = fs.writeFileSync
) {
	if (!folderName || !usersAmount || !messagesAmount || !rootFolder || !fileNames || !idName) {
		throw new Error("All parameter exept folderCallback and fileCallback are needed");
	}
	createFolder(folderName, rootFolder, folderCallback);
	createUserList(usersAmount, rootFolder, folderName, fileNames[0], fileCallback);
	const userList = require(`${rootFolder}/${folderName}/${fileNames[0]}.json`);
	createMessageList(
		messagesAmount,
		userList,
		rootFolder,
		folderName,
		fileNames[1],
		idName,
		fileCallback
	);
}

module.exports = {
	_getRandomNumber,
	_getRandomBoolean,
	createFolder,
	createUserList,
	createMessageList,
	generateDatas,
};
