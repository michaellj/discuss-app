// import models
const userModel = require("./user");

describe("userModel", () => {
	let user;
	beforeEach(() => {
		user = userModel();
	});
	afterEach(() => {
		user = null;
	});

	it("should return without error", () => {
		expect(user).not.toBeUndefined();
	});

	it("should return an object", () => {
		expect(typeof user).toBe("object");
	});

	it("should have an _id string of 20 character and starting with `usr-`", () => {
		const checkString = RegExp(/^usr-[\w]{16,}$/);

		expect(user._id).not.toBeUndefined();
		expect(checkString.test(user._id)).toBe(true);
	});

	it("should have a firstName property wich contains only alpha numerical characters", () => {
		const checkString = RegExp(/^[a-zA-Z]{2,}[-]?[[a-zA-Z]{2,}]?$/gm);
		expect(user.firstName).not.toBeUndefined();
		expect(checkString.test(user.firstName)).toBe(true);
	});

	it("should have a lastName property wich contains only alpha numerical characters", () => {
		const checkString = RegExp(/^[a-zA-Z]{2,}[-]?[[a-zA-Z]{2,}]?$/gm);
		expect(user.lastName).not.toBeUndefined();
		expect(checkString.test(user.lastName)).toBe(true);
	});

	it("should have an avatar property with a string value", () => {
		expect(user.pictureUrl).not.toBeUndefined();
		expect(typeof user.pictureUrl).toEqual("string");
	});
});
