"use strict";

const faker = require("faker");
const uniqid = require("uniqid");

/**
 * @function create a fake user object
 * @return {{_id: String, firstName: String, lastName: String, pictureUrl: String}}
 * returns a plain object which contains user's datas
 */
function user() {
	return {
		_id: uniqid("usr-"),
		firstName: faker.name.firstName(),
		lastName: faker.name.lastName(),
		pictureUrl: faker.internet.avatar(),
	};
}

module.exports = user;
