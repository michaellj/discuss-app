"use strict";

// import libraries
const faker = require("faker");
const uniqid = require("uniqid");

/**
 * @function publicMessage
 * @param {string} transmitterID
 * id of the person which create the message
 * @param {string} content
 * a string which contain the plain message
 * @returns {{_id: String, transmitter: String, recipient: (String|null), isPublic: Boolean, date: Object, content: String}}
 * returns a plain object which contains the message and its datas
 */
function publicMessage(transmitterID, content) {
	return {
		_id: uniqid("msg_"),
		transmitter: transmitterID,
		recipient: null,
		isPublic: true,
		date: faker.date.past(),
		content,
	};
}

/**
 * @function privateMessage
 * @param {string} transmitterID
 * id of the person which create the message
 * @param {string} recipientID
 * id of the person the message was sent to
 * @param {string} content
 * a string which contain the plain message
 * @returns {{_id: String, transmitter: String, recipient: (String|null), isPublic: Boolean, date: Object, content: String}}
 * returns a plain object which contains the message and its datas
 */
function privateMessage(transmitterID, recipientID, content) {
	return {
		_id: uniqid("msg_"),
		transmitter: transmitterID,
		recipient: recipientID,
		isPublic: false,
		date: faker.date.past(),
		content,
	};
}

module.exports = { publicMessage, privateMessage };
