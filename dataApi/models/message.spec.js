// import models
const messageModel = require("./message");

describe("publicMessage", () => {
	let message;
	const transmitterID = "usr-4xjvy59qkgnbivb1";
	const content = "test message content";

	beforeEach(() => {
		message = messageModel.publicMessage(transmitterID, content);
	});
	afterEach(() => {
		message = null;
	});

	it("should return without error", () => {
		expect(message).not.toBeUndefined();
	});

	it("should return an object", () => {
		expect(typeof message).toEqual("object");
	});

	it("should have an _id string of 20 character and starting with `usr-`", () => {
		const checkString = RegExp(/^msg_[\w]{16,}$/gm);
		expect(message._id).not.toBeUndefined();
		expect(checkString.test(message._id)).toBe(true);
	});

	it("should have a transmitter property which is the id of the emitter", () => {
		expect(message.transmitter).not.toBeUndefined();
		expect(message.transmitter).toEqual(transmitterID);
	});

	it("should have a recipient property equal to `null`", () => {
		expect(message.recipient).not.toBeUndefined();
		expect(message.recipient).toEqual(null);
	});

	it("should have a isPublic property equal to `true`", () => {
		expect(message.isPublic).not.toBeUndefined();
		expect(message.isPublic).toEqual(true);
	});

	it("should have a date property", () => {
		expect(message.date).not.toBeUndefined();
		expect(typeof message.date).toEqual("object");
	});

	it("should have a content property equal to the content passed", () => {
		expect(message.content).not.toBeUndefined();
		expect(message.content).toEqual(content);
	});
});

describe("privateMessage", () => {
	let message;
	const transmitterID = "usr-4xjvy59qkgnbivb1";
	const recipientID = "usr-4xjvy59qkgnbivbf";
	const content = "test message content";

	beforeEach(() => {
		message = messageModel.privateMessage(transmitterID, recipientID, content);
	});
	afterEach(() => {
		message = null;
	});

	it("should return without error", () => {
		expect(message).not.toBeUndefined();
	});

	it("should return an object", () => {
		expect(typeof message).toEqual("object");
	});

	it("should have an _id string of 20 character and starting with `usr-`", () => {
		const checkString = RegExp(/^msg_[\w]{16,}$/gm);

		expect(message._id).not.toBeUndefined();
		expect(checkString.test(message._id)).toBe(true);
	});

	it("should have a transmitter property which is the id of the emitter", () => {
		expect(message.transmitter).not.toBeUndefined();
		expect(message.transmitter).toEqual(transmitterID);
	});

	it("should have a recipient property equal to to the id of the recipient", () => {
		expect(message.recipient).not.toBeUndefined();
		expect(message.recipient).toEqual(recipientID);
	});

	it("should have a isPublic property equal to `false`", () => {
		expect(message.isPublic).not.toBeUndefined();
		expect(message.isPublic).toEqual(false);
	});

	it("should have a date property", () => {
		expect(message.date).not.toBeUndefined();
		expect(typeof message.date).toEqual("object");
	});

	it("should have a content property equal to the content passed", () => {
		expect(message.content).not.toBeUndefined();
		expect(message.content).toEqual(content);
	});
});
