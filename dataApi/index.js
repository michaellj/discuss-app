// import libraries
const path = require("path");
const fs = require("fs");
const api = require("./dataApi");

// declare variables to create datas
const folderName = "datas";
const rootFolder = path.dirname(__dirname) + "/src";
const fileNames = ["users", "messages"];

let userAmount, messageAmount;

if (process.argv[2] && process.argv[3]) {
	userAmount = parseInt(process.argv[2]);
	messageAmount = parseInt(process.argv[3]);

	if (isNaN(userAmount)) userAmount = 20;
	if (isNaN(messageAmount)) messageAmount = 50;
}

// run datas generation
api.generateDatas(
	folderName,
	userAmount,
	messageAmount,
	rootFolder,
	fileNames,
	"_id",
	fs.mkdirSync,
	fs.writeFileSync
);
