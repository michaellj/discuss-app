# Discuss app
A simple React chat application UI based on

**Summary**

- [Discuss app](#discuss-app)
	- [Technical test instructions](#technical-test-instructions)
	- [Technical informations](#technical-informations)
	- [Folders organisation](#folders-organisation)
	- [Usage](#usage)
		- [Datas](#datas)
			- [User model](#user-model)
			- [Messages models](#messages-models)
		- [Components](#components)
		- [Run application](#run-application)
	- [The Discuss Chat UI](#the-discuss-chat-ui)
	- [Tests](#tests)

## Technical test instructions
Develop an user interface capable of listing messages. It will also allow you to fill in a message to post it.

A message consists of a text and a field specifying whether it is private or public.

The approach should be API-centric, but there is no need to develop APIs for testing. Static data will be sufficient to populate pages.

Your development must be able to be easily picked up by another team member, he must have all the elements to understand your work easily.

You must take the best possible measures to guarantee the quality and functioning of your deliverable.

Please drop us the code on a repository of your choice that is accessible so that we can look at it.

<sub>[Go to summary](#discuss-app)</sub>

## Technical informations


**Before you start coding in this repository you should check that everything you need is installed on your computer :**

| language | minimum version | link to documentation |
| -------- | --------------- | --------------------- |
| Node.js  | 14.8.0          | [documentation](https://nodejs.org/en/) |
| Yarn     | 1.22.10         | [documentation](https://yarnpkg.com/getting-started/install) |
| Npm      | 7.0.8           | [documentation](https://www.npmjs.com/get-npm) |


<sub>[Go to summary](#discuss-app)</sub>

## Folders organisation

Let's take a look at the basic directory's work tree :

```console
.[root folder]
|
|-- dataApi
|	|-- models
|	|	|-- messages.js
|	|	|-- messages.spec.js 
|	|	|-- users.js
|	|	|-- users.spec.js
|	|
|	|-- dataApi.js
|	|-- dataApi.spec.js
|	|-- index.js
|
|-- public
|	|-- favicon.ico
|	|-- index.html
|	|-- logo.png
|	|-- main.css
|	|-- main.css.map
|	|-- manifest.json
|	|-- robots.txt
|
|-- src
|	|-- api
|	|	|-- api.spec.js
|	|	|-- index.js
|	|
|	|-- components
|	|	|-- ChatButton
|	|	|	|-- ChattButton.spec.js
|	|	|	|-- index.js
|	|	|
|	|	|-- ChatContent
|	|	|	|-- ChatContent.spec.js
|	|	|	|-- index.js
|	|	|
|	|	|-- Chatsection
|	|	|	|-- ChatSection.spec.js
|	|	|	|-- index.js
|	|	|
|	|	|-- Form
|	|	|	|-- Form.spec.js
|	|	|	|-- index.js
|	|	|
|	|	|-- Header
|	|	|	|-- Header.spec.js
|	|	|	|-- index.js
|	|	|
|	|	|-- MainUserRow
|	|	|	|-- MainUserRow.spec.js
|	|	|	|-- index.js
|	|	|
|	|	|-- Message
|	|	|	|-- Message.spec.js
|	|	|	|-- index.js
|	|	|
|	|	|-- ProfileImage
|	|	|	|-- ProfileImage.spec.js
|	|	|	|-- index.js
|	|	|
|	|	|-- UserList
|	|	|	|-- UserList.spec.js
|	|	|	|-- index.js
|	|	|
|	|	|-- UserRow
|	|	|	|-- UserRow.spec.js
|	|	|	|-- index.js
|	|	|
|	|	|-- UserSection
|	|	|	|-- UserSection.spec.js
|	|	|	|-- index.js
|	|	|
|	|	|-- App.js
|	|	|-- App.spec.js
|	|
|	|-- contexts
|	|	|-- chatPrivacyContext.js
|	|	|-- chatPrivacyContext.spec.js
|	|	|-- messagesContext.js
|	|	|-- messagesContext.spec.js
|	|	|-- userContext.js
|	|	|-- userContext.spec.js
|	|
|	|-- datas
|	|	|-- messages.json
|	|	|-- users.json
|	|
|	|-- style
|	|	|-- abstract
|	|	|	|-- __colors.scss
|	|	|	|-- __fonts.scss
|	|	|	|-- __functions.scss
|	|	|	|-- __mixins.scss
|	|	|	|-- __spacing.scss
|	|	|	|-- __variables.scss
|	|	|	|-- _abstract.scss
|	|	|
|	|	|-- base
|	|	|	|-- __fonts.scss
|	|	|	|-- __utilities.scss
|	|	|	|-- _base.scss
|	|	|
|	|	|-- components
|	|	|	|-- __buttons.scss
|	|	|	|-- __chat.scss
|	|	|	|-- __message.scss
|	|	|	|-- __row.scss
|	|	|	|-- __user.scss
|	|	|	|-- _components.scss
|	|	|
|	|	|-- layouts
|	|	|	|-- __app.scss
|	|	|	|-- __body.scss
|	|	|	|-- __chat.scss
|	|	|	|-- __user.scss
|	|	|	|-- _layouts.scss
|	|	|
|	|	|-- reset
|	|	|	|-- __antialiasing.scss
|	|	|	|-- __reset-stylesheet.scss
|	|	|	|-- _reset.scss
|	|	|
|	|	|-- main.scss
|	|
|	|-- index.js
|	|-- setupTests.js
|
|-- utils
|	|-- tests.js
|
|-- .gitignore
|-- jsconfig.json
|-- README.md
|-- yarn.lock
```

<sub>[Go to summary](#discuss-app)</sub>

## Usage

Clone or download the repository and install dependencies

```console
$ cd discuss-app
$ yarn install
```

### Datas
Used datas are presents in `./src/datas/` folder. You will find within both messages.json and users.json files.

If you want to generate some other datas you can with the cli command
```console
$ yarn datas <number_of_users> <number_of_messages>
```

You can create as much datas as you want datas api. It will generate random public and private messages depending on users list generated.

For example :

```console
$ yarn datas 50 100
```
Will give you 50 users with 100 messages including public and private messages.

These datas will be consumed within the application

#### User model

A user is an object which contains the following key and values :
```javascript
const user = {
	_id: String,
	firstName: String,
	lastName: String,
}
```

#### Messages models

A message is an object which contains the following key and values :

**Public message**
```javascript
const publicMessage = {
	_id: String,
	transmitter: String // a user _id value,
	recipient: null // public messages don't need this value,
	isPublic: true,
	date: new Date(),
	content: String // the message himself

}
```

**Private message**
```javascript
const privateMessage = {
	_id: String,
	transmitter: String // a user _id value,
	recipient: String // a user _id value,
	isPublic: false,
	date: new Date(),
	content: String // the message himself
}
```

<sub>[Go to summary](#discuss-app)</sub>

### Components
Each component is included in its own folder with its test file (Component.spec.js). The components are written in the index.js files in order to simplify the writing of their imports.

So rather than writing
```javascript
import MyComponent from './MyComponent/MyComponent';
```
just write
```javascript
import MyComponent from './MyComponent';
```
<sub>[Go to summary](#discuss-app)</sub>

### Run application

As this app was created with `create-react-app` you can simply run
```console
$ yarn start
```

The application will run on `localhost:3000` by default if it is available or a production port.

<sub>[Go to summary](#discuss-app)</sub>

## The Discuss Chat UI
There is 2 chat mode with Discuss UI : public and private.

By default, app start on the **public chat mode** and let you post **public message**.

By **clicking on a UserRow** component, you switchin on **private mode** and your messages become private between you and your correspondant.

<sub>[Go to summary](#discuss-app)</sub>
## Tests

To run test in watch mode simply run

```console
$ yarn test
```

To know test coverage run

```console
$ yarn test --coverage --watchAll=false
```

To run data api tests run
```console
$ yarn jest dataApi --watchAll
```
<sub>[Go to summary](#discuss-app)</sub>
___

Thanks for reading

**Mika**